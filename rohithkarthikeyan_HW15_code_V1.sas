/***********************************************************************************/
/* Program Name: rohithkarthikeyan_HW15_code_V1.sas*/
/* Date Created: 11/27/2017 */
/* Author: Rohith Karthikeyan*/
/* Purpose: HW15 - STAT 604 */
/***********************************************************************************/

/*--------------------------
 Step 1 - Libname Statements 
----------------------------*/

LIBNAME srcData '/folders/myfolders/PRG1_Data_and_Programs/PRG1 Data and Programs (New)/' ACCESS = READONLY;
LIBNAME rkRef '/folders/myfolders/HW15/';
filename srcFile '/folders/myfolders/PRG1_Data_and_Programs/PRG1 Data and Programs (New)/OKSchools.csv';

/*--------------------------
 Step 2 - Output Formatting 
----------------------------*/

options orientation = landscape nodate pageno = 2;
ods escapechar='\';
filename outFile '/folders/myfolders/HW14/outFileRK.pdf';
ods pdf file = outFile;

/*--------------------------
 Step 3&4 - Create them formats 
----------------------------*/

proc format;
value schlDiv 	1251-high = '6A'
				721-1250 = '5A'
				375-720 = '4A'
				181-374 = '3A'
				107-180 = '2A'
				70-106 = 'A'
				0-69 = 'B'
				. = 'Non-HS'
				other = 'Non-HS';
value  strRatFmt 	low-<10 = 'Very Small'
					10-<14 = 'Small'
					14-<18 = 'Medium'
					18-<22 = 'Large'
					22-high = 'Very Large'
					. = 'Unkown';
run;

/*--------------------------
 Step 5 - Create them data 
----------------------------*/

data work.OKSchoolData;
 length county $24 School $30;
 infile srcFile dlm=',' dsd missover firstobs=2;
 input School $ LocCity $
 MailCity $ County $ Teachers Grade7 Grade8 
 GRade9 Grade10 Grade11 Grade12 Ungraded PreTotal 
 ElemTotal HSTotal STRatio;
 run;
 
 /*--------------------------
 Step 6 - Print them data 
----------------------------*/
 
proc print data = OKSchoolData(firstobs = 1 obs=30) noobs label;
label county = 'County';
var School LocCity MailCity County Teachers Grade7 Grade8 
 GRade9 Grade10 Grade11 Grade12 Ungraded PreTotal 
 ElemTotal HSTotal STRatio;
 title 'Oklahoma School Analysis\n';
 title2 'Partial Listing';
 footnote 'Based on NCES Data';
run;

 /*--------------------------
 Step 7 - Freq procedure 
----------------------------*/

proc freq data=OKSChoolData;
 tables STRatio/Missing sparse NOCUM NOCOl;
 format STRatio strRatFmt.;
 title2 'Distribution of Class Sizes Based on Student/Teacher Ratio';
 label;
run;

 /*--------------------------
 Step 8 - Summary procedure 
----------------------------*/

proc summary data = OKSchoolData mean MAXDEC=1 MISSING;
format HSTotal schlDiv.;
var STRatio;
Class HSTotal;
output out = OKSchoolData2 mean=;
run;

 /*--------------------------
 Step 9 - Print Summary 
----------------------------*/
proc print data = OKSChoolData2(keep = HSTotal _FREQ_ STRatio firstobs=2) NOOBS LABEL ;
options date;
label _FREQ_ = 'School' HSTotal = 'Division' STRatio = 'Ratio';
title2 '\nAverage Student-Teacher Ratio by School Division';
format STRatio COMMA8.1;
run;

 /*--------------------------
 Step 10 - House Keeping 
----------------------------*/

title;
title2;
footnote;

ods pdf close;
*That's all folks!;
