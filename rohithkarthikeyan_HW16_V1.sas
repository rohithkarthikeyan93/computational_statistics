/***********************************************************************************
Program Name: rohithkarthikeyan_HW15_code_V1.sas
Date Created: 12/05/2017 
Author: Rohith Karthikeyan
Purpose: HW16 - STAT 604 
/***********************************************************************************/

/*--------------------------
 Step 1 - Libname Statements 
----------------------------*/

LIBNAME srcData '/folders/myfolders/PRG1_Data_and_Programs/PRG1 Data and Programs (New)/' ACCESS = READONLY;
LIBNAME rkRef '/folders/myfolders/HW16/';
filename srcFile '/folders/myfolders/PRG1_Data_and_Programs/PRG1 Data and Programs (New)/andromeda.dat';

/*--------------------------
 Step 2 - Data Step 
----------------------------*/

data work.Andromeda;

 infile srcFile;
 input levelVal $8. @1 
 		 @10 Job_Title $24.
 	     @106 Salary $10.@;
 if levelVal ='' then
 input  @10 levelVal $8. 
 	    @19 Job_Title $19.@;	    
 if levelVal = '' then
 input 	@19 levelVal $8.
 		@28 Job_Title $14. @;
 if levelVal = '' then
 input 	@28 levelVal $8.
 		@37 Job_Title $8. @;
 if levelVal = '' then
 input 	@37 levelVal $8.
 		@46 Job_Title $13. @;
 if levelVal = '' then
 input 	@46 levelVal $8.@
 		@55 Job_Title $14. @;;
 
 run;