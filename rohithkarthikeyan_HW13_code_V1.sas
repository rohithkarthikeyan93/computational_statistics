/***********************************************************************************/
/* Program Name: rohithkarthikeyan_HW13_code_V1.sas*/
/* Date Created: 11/13/2017 */
/* Author: Rohith Karthikeyan*/
/* Purpose: HW13 - STAT 604 */
/***********************************************************************************/

/*--------------------------
 Step 1 - Libname Statements 
----------------------------*/

LIBNAME srcData '/folders/myfolders/PRG1_Data_and_Programs/PRG1 Data and Programs (New)/' ACCESS = READONLY;
LIBNAME rkRef '/folders/myfolders/HW13/';

filename outFile '/folders/myfolders/HW13/outFileRK.pdf';
ods pdf file = outFile;
/*--------------------------
 Step 2 - Narrow Dataset 
----------------------------*/

data NarrowSet(keep = student_id fund_code i);
set srcData.scholarships (keep = student_id fund1-fund10);
array fundSrc{*} fund:;
iter = dim(fundSrc);
	do i =1 to iter;
		if fundSrc{i} ne . then do;
			i=i;
			fund_code=fundSrc{i};
		output;
		end;
	end;
run;

/*--------------------------
 Step 3 - Sort for merging 
----------------------------*/

proc sort data = work.NarrowSet out = narrowSorted;
by fund_code;
run;

/*--------------------------
 Step 4 - Sort fund data 
----------------------------*/

proc sort data = srcData.fund_data out = fundSorted;
by fund_code;
run;

/*--------------------------
 Step 5 - Match merge 
----------------------------*/

data mergedData (keep = student_id i fund_code Category);
merge narrowSorted(in = NS) fundSorted(in =FS);
by fund_code;
if NS =1 and FS=1;
run;

proc sort data = mergedData out = sortMerged;
by student_id;

run;

/*--------------------------
 Step 6 - Transpose
----------------------------*/

proc transpose data = work.sortMerged out = transposedMerge(drop = _NAME_ _LABEL_) prefix = Fund_Type;
by student_id;
var category;
run;

/*--------------------------
 Step 7 - Single Data Step
----------------------------*/

data finalSet;
 
merge transposedMerge srcData.scholarships;
by student_id;
array Val{*} amount:;
iter2 = dim(Val);
array fundType{*} Fund_Type:;
intValRun = 0;
atValRun = 0;
do i =1 to iter2;
if fundType{i} = 'Internal' then do;
intValRun = sum(Val{i},intValRun);
end;
if fundType{i} = 'Athletic' then do;
atValRun = sum(Val{i},atValRun);
end;
end;
netTotal = sum(of amount1-amount10);
label intValRun = 'Internal Scholarships' atValRun = 
'Athletic Scholarships' netTotal = 'Total aid' major = 'Maj_Code';
run;

/*--------------------------
 Step 7 - Print Stuff
----------------------------*/

proc contents data=work.finalSet VARNUM; 
run;

proc print data=work.finalSet NOOBS LABEL;
var student_id Name major intValRun atValRun netTotal;
run;

ods pdf close;
