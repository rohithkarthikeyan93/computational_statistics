/***********************************************************************************/
/* Program Name: rohithkarthikeyan_HW11_code_V1.sas*/
/* Date Created: 10/25/2017 */
/* Author: Rohith Karthikeyan*/
/* Purpose: HW11 - STAT 604 */
/***********************************************************************************/

/*--------------------------
 Step 1 - Libname Statement 
----------------------------*/

LIBNAME srcData '/folders/myfolders/PRG1_Data_and_Programs/PRG1 Data and Programs (New)/' ACCESS = READONLY;

/*--------------------------
 Step 2 - Narrow DataSet 
----------------------------*/

data narrowData;
set srcData.jobs2017;
* Convert sector name;
if Sector = 'PROFESSIONAL AND BUSINESS SERVICES' then do; 
Sector = 'PROFESSIONAL/BUSINESS SERVICES';
end;
* Convert sector names to proper case;
Sector = propcase(Sector);

*Create/ append to dataset w/ conditions as specified;

if Aug__2016 ~= '';
Month = 'August';
Year = 2016;
Jobs = Aug__2016;
output;

if Sept__2016 ~= '';
Month = 'September';
Year = 2016;
Jobs = Sept__2016;
output;

if Oct__2016 ~= '';
Month = 'October';
Year = 2016;
Jobs = Oct__2016;
output;

if Nov__2016 ~= '';
Month = 'November';
Year = 2016;
Jobs = Nov__2016;
output;

if Dec__2016 ~= '';
Month = 'December';
Year = 2016;
Jobs = Dec__2016;
output;

if Jan__2017 ~= '';
Month = 'January';
Year = 2017;
Jobs =Jan__2017;
output;

if Feb__2017 ~= '';
Month = 'February';
Year = 2017;
Jobs =Feb__2017;
output;

if Mar__2017 ~= '';
Month = 'March';
Year = 2017;
Jobs =Mar__2017;
output;

if Apr__2017 ~= '';
Month = 'April';
Year = 2017;
Jobs =Apr__2017;
output;

if May_2017 ~= '';
Month = 'May';
Year = 2017;
Jobs =May_2017;
output;

if June_2017 ~= '';
Month = 'June';
Year = 2017;
Jobs =June_2017;
output;

if July_2017 ~= '';
Month = 'July';
Year = 2017;
Jobs =July_2017;
output;

if Aug__2017 ~= '';
Month = 'August';
Year = 2017;
Jobs =Aug__2017;
output;

keep Sector State Jobs Year Month;
run;

/*--------------------------
 Step 3 - 6 Temp DataSets 
----------------------------*/

data largeSet(keep=Sector State avgJobs)
mediumSet(keep=Sector State avgJobs) 
smallSet(keep=Sector State avgJobs) 
government(keep=State avgJobs marketSize)
goods(keep=Sector State avgJobs marketSize)
services(keep=Sector State avgJobs marketSize);

set srcData.monthly_jobs1617;
*remove unwanted variables;
drop rep_date ann_chg;
*determine average;
avgJobs = sum(Aug__2016,Sept__2016,Oct__2016,Nov__2016,Dec__2016, Jan__2017, Feb__2017, Mar__2017, Apr__2017, May_2017, June_2017, July_2017,Aug__2017)/13;
*set to a single decimal format;
format avgJobs COMMA8.1; 
*apply appropriate label;
label avgJobs = 'Average Jobs';

*conditionals for market size attribute;
if avgJobs > 900 then marketSize = 'Large';
else if avgJobs > 100 AND avgJobs < 900 then marketSize = 'Medium';
else marketSize = 'Small';

*output to dataset conditionals;
if marketSize = 'Large' then output largeSet;
else if  marketSize = 'Small' then output smallSet;
else output mediumSet;

*select statements for dataset creation;
select(Sector);
when ('GOVERNMENT') do;
output government;
end;
when ('MANUFACTURING','CONSTRUCTION') do;
output goods;
end;
when('FINANCIAL ACTIVITIES',
'PROFESSIONAL AND BUSINESS SERVICES', 
'EDUCATION AND HEALTH SERVICES','LEISURE AND HOSPITALITY') do;
output services;
end;
*handles exceptions;
otherwise;
end;
run;


/*--------------------------
 Step 4 - Create pdf o/p file
----------------------------*/

*sets file path for o/p pdf ;
filename outFile '/folders/myfolders/HW11/outFileRK.pdf';
ods pdf file = outFile bookmarkgen=yes bookmarklist= hide;

/*--------------------------
 Step 5 - Print Statements
----------------------------*/

*Print 1;
title '5.1 - First 50 Observations from Monthly Jobs Data Set';
proc print data=work.narrowData(firstobs = 1 obs = 50) NOOBS LABEL;
run;

*Print 2;
title '5.2 - Last 50 Observations from Monthly Jobs Data Set';
proc print data=work.narrowData(firstobs = 5385 obs = 5434) NOOBS LABEL;
run;

*Print 3;
title '5.3 - Fifty Observations from Monthly Jobs Data Set Beginning with #2800';
proc print data=work.narrowData(firstobs = 2800 obs = 2849) NOOBS LABEL;
run;

/*--------------------------
 Step 6 - More Print Statements
----------------------------*/

*Print 6a;
title '6a - First 30 Observations of Small Markets';
proc print data=work.smallSet(firstobs = 1 obs = 30) LABEL;
run;

*Print 6b;
title '6b - First 30 Observations of Medium Markets';
proc print data=work.mediumSet(firstobs = 1 obs = 30) LABEL;
run;

*Print 6c;
title '6c - Large Markets';
proc print data=work.largeSet LABEL;
run;

*Print 6d;
title '6d - Selected Observations from Goods Sector';
proc print data=work.goods(firstobs = 75 obs = 104) NOOBS LABEL;
run;

*Print 6e;
title '6e - Small Markets in the Services Sector';
proc print data=work.services(firstobs = 1 obs = 30) LABEL;
where marketSize = 'Small';
run;

*Print 6f;
title 'Government Sector';
proc print data=work.government LABEL;
where marketSize = 'Small';
run;

/*-----------------------------
 Step 7 - SASHELP.VTABLE Print
-------------------------------*/
title '7 - Datasets in the Work Library';
proc print data= SASHELP.VTABLE(keep = libname memname crdate nobs nvar);
where libname = 'WORK';
run;

ods pdf close;

*That's all folks!Phew.;