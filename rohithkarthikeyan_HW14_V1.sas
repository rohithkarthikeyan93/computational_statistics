/***********************************************************************************/
/* Program Name: rohithkarthikeyan_HW14_code_V1.sas*/
/* Date Created: 11/19/2017 */
/* Author: Rohith Karthikeyan*/
/* Purpose: HW14 - STAT 604 */
/***********************************************************************************/

/*--------------------------
 Step 1 - Libname Statements 
----------------------------*/

LIBNAME srcData '/folders/myfolders/PRG1_Data_and_Programs/PRG1 Data and Programs (New)/' ACCESS = READONLY;
LIBNAME rkRef '/folders/myfolders/HW14/';

/*--------------------------
 Step 2 - Merge thy data
----------------------------*/

data receiversTemp(drop = W_L);
length Team $29;
set srcData.receivers17;
Team = scan(Player,-1,',');
Team = Strip(Team);
run;

data totOffenseTemp(drop = W_L);
set srcData.totaloffense17;
run;

proc sort data = work.receiversTemp out = recSort;
by Team;
run;

proc sort data = totOffenseTemp out = totOffSort;
by Team;
run;

data rkRef.ALLDATA work.TEAMDATA(drop = newVar) work.NORECV
(drop = rank player Cl pos games newVar rec_yds rec tds yds_game percentVal);
Merge totOffSort(in = TS rename =(Rank = TeamRank
games = TeamGames TDs = TeamTDs Yds_Game = TeamYds_Game )) recSort(in = RS);
by Team;
if RS = 1 then newVar = 'Yes';
else newVar = 'No';
percentVal  = YDS_Game/TEAMYDS_GAME;
output rkRef.ALLDATA;
format percentVal PERCENT8.2;

if TS=1 and RS =0 then
output work.NORECV;

if RS = 1 and TS = 1;
output work.TEAMDATA;

run;

/*--------------------------
 Step 3 - Output Formatting
----------------------------*/

options orientation = landscape;
options dtreset;
options nonumber;
filename outFile '/folders/myfolders/HW14/outFileRK.pdf';
ods pdf file = outFile;
ods escapechar='\';
/*--------------------------
 Step 4 - Reorder Norecv
----------------------------*/

proc sort data = work.norecv;
by teamrank;
run;

/*--------------------------
 Step 5 - Fancy prints
----------------------------*/
proc print data = norecv(firstobs=1 obs=10) LABEL NOOBS;
var TeamRank Team plays yds yds_play teamyds_game;
label TeamRank = 'Rank' plays = 'Total Plays' yds = 'Total Yards'
yds_play = 'Yards per Play' teamyds_game = 'Yards per Game';
format teamyds_game COMMA8.;
title'NCAA Football Receiving Analysis \n \n
Top 10 Offences with No Top Receivers';
footnote 'Data Downloaded from NCAA.org';
run;

/*--------------------------
 Step 6 - No more time
----------------------------*/
options nodtreset;
/*--------------------------
 Step 7 - Freq Procedure
----------------------------*/
footnote;
title;
proc freq data=rkref.alldata;
 tables Cl*pos/Missing sparse NOPERCENT NOCOl;
 output out = work.freq2;
 label Cl = 'Class' Pos = 'Position';
 title2 'NCAA Football Receiving Analysis \n
Top 10 Offences with No Top Receivers';
run;
/* proc print data=freq2; */
/* run; */

/*--------------------------
 Step 8 - Means procedure
----------------------------*/
title2;
proc means data=teamdata mean median Q1 Q3 MAXDEC=2;
title'NCAA Football Receiving Analysis \n \n
Top 10 Offences with No Top Receivers';
 var percentVal;
 class cl pos;
 format Q1 Q3 mean median COMMA8.2;
run;

/*--------------------------
 Step 9 - Tabulate procedure
----------------------------*/

proc tabulate data=teamdata Schools Ratio;
class cl pos;
var percentVal;
table cl*pos all,percentVal*(N mean median Q1 Q3);
run;
ods pdf close;
*Thanksgiving break w/mana!;